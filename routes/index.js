var controllers = require('../controllers/all-controllers');
module.exports = function (app, passport) {
	app.get('/', controllers.frontAppController.index);
	app.post('/', passport.authenticate('local-login', {
		successRedirect: '/dashboard', // redirect to the secure profile section
		failureRedirect: '/', // redirect back to the signup page if there is an error
		failureFlash: true // allow flash messages
	}));
	app.get('/dashboard', isLoggedIn, controllers.frontAppController.dashboard);
	app.get('/submerchant', isLoggedIn, controllers.submerchantController.list);
	app.get('/token', isLoggedIn, controllers.tokenControllers.list);
	app.get('/customer', isLoggedIn, controllers.customerController.list);
	app.get('/prepayment', isLoggedIn, controllers.transactionController.listPrepayment);
	app.get('/postpayment', isLoggedIn, controllers.transactionController.listPostPayment);
	//
	app.post('/generatesubmerchant', controllers.submerchantController.create);
	app.get('/submerchant/:id', controllers.submerchantController.read);
	app.post('/submerchant/report/transaction', controllers.reportController.reportControllerOnDemand);
	app.post('/submerchant/transaction/search', controllers.transactionController.searchTransaction);
	app.post('/submerchant/transaction/searchById', controllers.transactionController.searchTransactionById);
	app.post('/submerchant/:id', controllers.submerchantController.read);
	app.post('/submerchant/list', controllers.submerchantController.list);
	app.post('/webhook/submerchant/receive', controllers.submerchantWebhookController.create);
	app.post('/customer/create', controllers.customerController.create);
	app.post('/customer/transaction/create', controllers.transactionController.create);
	app.post('/customer/transaction/requestToken', controllers.tokenControllers.create);
	app.post('/customer/transaction/history', controllers.transactionController.transactionHistory);
	app.post('/report/transaction', controllers.reportController.transactionByStatus);
	app.get('/logout', function (req, res) {
		req.logout();
		req.session.destroy();
		res.redirect('/');
	});
}

// route middleware to ensure user is logged in
function isLoggedIn(req, res, next) {
	if (!req.user) {
		res.redirect('/');
	} else {
		next();
	}
}