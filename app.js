var express = require('express');
var path = require('path');
var braintree = require("braintree");
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var passport = require('passport');
var flash = require('flash');
var bodyParser = require('body-parser');
var models = require('./models/all-models').toContext(global);
var output = require('./middleware/jsonOutput');
var config = require('config');
var session = require('express-session');
var redis = require("redis");
var client = redis.createClient();
var redisStore = require('connect-redis')(session);
var mailgun = require('mailgun-js');
var moment = require('moment');
var swig = require('swig');
var models = require('./models/all-models');
swig.setDefaults({
	cache: false
});
console.log(swig.version);
/**
 * app is for API
 * @return {[type]} [description]
 */
var app = express();
/**
 * front is for Frontend Monitoring
 * @return {[type]} [description]
 */ // check if menu is selected
app.locals.isActive = function (strMenu, strState, strReturn) {
	//console.log('strState');
	if (typeof strReturn === 'undefined') strReturn = 'active';
	return (strMenu === strState) ? strReturn : 'false';
	//return 'active';
}

app.locals.trim = function (string) {
	var maxLength = 50 // maximum number of characters to extract
	var trimmedString = string.substr(0, maxLength);
	trimmedString = trimmedString.substr(0, Math.min(trimmedString.length, trimmedString.lastIndexOf(" ")))
	if (string.length < maxLength) {
		return string;
	} else {
		return trimmedString + ' ...';
	}
}

app.locals.titleCase = function (str) {
	return str.replace(/_/g, ' ').replace(/\w\S*/g, function (txt) {
		return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
	});
}

require('./middleware/passport')(passport); // pass passport for configuration
var config = require('config');
var configuration = config.get('tera');

var gateway = braintree.connect({
	environment: braintree.Environment.Sandbox,
	merchantId: config.get('tera').braintree.merchantId,
	publicKey: config.get('tera').braintree.publicKey,
	privateKey: config.get('tera').braintree.privateKey,
	masterMerchant: config.get('tera').braintree.masterMerchant
});

var sessionOpts = {
	resave: true,
	saveUninitialized: true,
	store: new redisStore({
		host: configuration.redis.host,
		prefix: "tera:",
		port: configuration.redis.port,
		client: client,
		ttl: configuration.redis.ttl
	}), // connect-mongo session store
	saveUninitialized: false,
	secret: configuration.cookie.secret,
}

var mailgun = new mailgun({
	apiKey: config.get('tera').mailgun.apiKey,
	domain: config.get('tera').mailgun.domain
});

app.set('model', models);
app.set('output', output);
app.set('gateway', gateway);
app.set('braintree', braintree);
app.set('mailgun', mailgun);

app.locals.standarizeCalendarInput = function (date) {
	return moment(date).format('YYYY-MM-DD')
}

app.set('views', path.join(__dirname, 'views'));
var swigs = new swig.Swig();

app.engine('html', swigs.renderFile);
app.set('view engine', 'html');
app.use(logger('dev'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
	extended: true
}));

app.use(cookieParser());
app.use(session(sessionOpts));
app.use(passport.initialize());
app.use(passport.session(sessionOpts));
app.use(flash()); // use connect-flash for flash messages stored in session
app.use(express.static(path.join(__dirname, 'public')));
var uri = config.get('tera').dbConfig.host;

require('./routes/index.js')(app, passport); // load our routes and pass in our app and fully configured passport
app.use(function (req, res, next) {
	var err = new Error('Not Found');
	err.status = 404;
	next(err);
});

if (app.get('env') === 'development') {
	app.use(function (err, req, res, next) {
		res.status(err.status || 500);
		res.render('error', {
			message: err.message,
			error: err
		});
	});
}
app.use(function(req, res, next) {
  if (req.session && req.session.user) {
    models.user.findOne({ email: req.session.user.email }, function(err, user) {
      if (user) {
        req.user = user;
        delete req.user.password; // delete the password from the session
        req.session.user = user;  //refresh the session value
        res.locals.user = user;
      }
      // finishing processing the middleware and run the route
      next();
    });
  } else {
    next();
  }
});

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
	res.status(err.status || 500);
	res.render('error', {
		message: err.message,
		error: {}
	});
});


module.exports = app;
