var EmailTemplate = require('email-templates').EmailTemplate
var path = require('path')
var templatesDir = path.resolve(__dirname, '../', 'template')
module.exports = {
	sendSucccessEmail: function (req, context) {
		console.log(context);
		var template = new EmailTemplate(path.join(templatesDir, 'receipt/success/'));
    console.log(context['customFields']['paymentname']);
    var locals = {
      req: context
    }
		template.render(locals,function (err, result) {

			var data = {
				from: 'no-reply@Masjidpay.com',
				to: context['customer']['email'],
				subject: "Billing Success",
				html: result.html
			};
			console.log(req.app.settings.mailgun);
			req.app.settings.mailgun.messages().send(data, function (error, body) {
				console.log('err' + error);
				console.log(body);
			});
		})
	},
}
