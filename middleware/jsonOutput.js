module.exports = {
	successResult: function (res, status, successResult) {
		console.log(status);
		return res.json({
			status: status ? status : "success",
			message: successResult
		})
	},
	errorResult: function (res, statusCode, successResult) {
		return res.json(parseInt(statusCode), {
			status: "error",
			message: successResult
		})
	}
}
