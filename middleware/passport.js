var LocalStrategy = require('passport-local').Strategy;
var models = require('../models/all-models');
// // load the auth variables
// var configAuth = require('./auth'); // use this one for testing

module.exports = function (passport) {
	passport.serializeUser(function (user, done) {
		done(null, user.id);
	});

	// used to deserialize the user
	passport.deserializeUser(function (id, done) {
		models.user.findById(id, function (err, user) {
			done(err, user);
		});
	});

	passport.use('local-login', new LocalStrategy({
			// by default, local strategy uses username and password, we will override with email
			usernameField: 'email',
			passwordField: 'password',
			passReqToCallback: true // allows us to pass in the req from our route (lets us check if a user is logged in or not)
		},
		function (req, email, password, done) {
			if (email)
				email = email.toLowerCase(); // Use lower-case e-mails to avoid case-sensitive e-mail matching
			// asynchronous
			process.nextTick(function () {
				models.user.findOne({
					'email': email
				}, function (err, user) {
					// if there are any errors, return the error
					if (err)
						return done(err);
					// if no user is found, return the message
					if (!user)
						return done(null, false, req.flash('loginMessage', 'No user found.'));
					if (!user.validPassword(password))
						return done(null, false, req.flash('loginMessage', 'The password you\'ve entered is incorrect.'));
					else
					req.app.locals.user = undefined;
					var users = new Object;
					users = user;
					delete users.password
					req.app.locals.user = users;
						return done(null, user);
				});
			});

		}));

};
