var mongoose = require('mongoose');
var bcrypt = require('bcrypt');
var Schema = mongoose.Schema;
var SALT_WORK_FACTOR = 10;
if (mongoose.connection.readyState === 0) {
	mongoose.connect(require('./connection-string'));
}

var newSchema = new Schema({

	/**
	 * username here using email
	 */
	'email': {
		type: String,
		require: true,
		unique: true
	},
	'password': {
		type: String
	},
	'createdAt': {
		type: Date,
		default: Date.now
	},
	'updatedAt': {
		type: Date,
		default: Date.now
	}
});

newSchema.pre('save', function (next) {
	var user = this;
	var currentDate = new Date();

	// change the updated_at field to current date
	user.updatedAt = currentDate;

	// if created_at doesn't exist, add to that field
	if (!user.createdAt)
		user.createdAt = currentDate;

	// only hash the password if it has been modified (or is new)
	if (!user.isModified('password')) return next();

	// generate a salt
	bcrypt.genSalt(SALT_WORK_FACTOR, function (err, salt) {
		if (err) return next(err);

		// hash the password using our new salt
		bcrypt.hash(user.password, salt, function (err, hash) {
			if (err) return next(err);
			user.password = hash;
			next();
		});
	});
});

newSchema.pre('update', function () {
	this.update({}, {
		$set: {
			updatedAt: Date.now()
		}
	});
});

newSchema.pre('findOneAndUpdate', function () {
	this.update({}, {
		$set: {
			updatedAt: Date.now()
		}
	});
});
newSchema.methods.generateHash = function (password) {
	return bcrypt.hashSync(password, bcrypt.genSaltSync(SALT_WORK_FACTOR), null);
};

// checking if password is valid
newSchema.methods.validPassword = function (password) {
	return bcrypt.compareSync(password, this.password);
};


module.exports = mongoose.model('user', newSchema);
