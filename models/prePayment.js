var mongoose = require('mongoose');
var Schema = mongoose.Schema;
if (mongoose.connection.readyState === 0) {
	mongoose.connect(require('./connection-string'));
}


var prePaymentSchema = new Schema({
	'createdAt': {
		type: Date,
		default: Date.now
	},
	'updatedAt': {
		type: Date,
		default: Date.now
	},
	'payment': {
		'id': {
			type: String
		},
		'amount': {
			type: String
		},
		'name': {
			type: String
		},
		'type': {
			type: String
		},
		'remarks': {
			type: String
		},
		'fee': {
			type: String
		},
		'braintreeToken': {
			type: String,
		},
		'nonce': {
			type: String,
		},
		'projectId': {
			type: String
		},
		'projectName': {
			type: String
		},
	},
	'customer': {
		'token': {
			type: String
		},
		'firstName': {
			type: String
		},
		'email': {
			type: String
		},
		'source': {
			type: String,
		},
		'lastName': {
			type: String
		},
		'id': {
			type: String
		},
		'token': {
			type: String,
		}
	},
	'submerchant': {
		'braintreeId': {
			type: String
		},
		'name': {
			type: String
		},
		'id': {
			type: String
		},
	}
});
prePaymentSchema.pre('save', function (next) {
	this.updatedAt = Date.now();
	next();
});

prePaymentSchema.pre('update', function () {
	this.update({}, {
		$set: {
			updatedAt: Date.now()
		}
	});
});

prePaymentSchema.pre('findOneAndUpdate', function () {
	this.update({}, {
		$set: {
			updatedAt: Date.now()
		}
	});
});



module.exports = mongoose.model('prePayment', prePaymentSchema);
