// grab the things we need
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

var postPaymentSchema = new Schema({
	any: Schema.Types.Mixed
}, {
	strict: false

}, {
	autoIndex: true
});


// make this available to our users in our Node applications
module.exports = mongoose.model('postPayment', postPaymentSchema);
