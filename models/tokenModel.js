var mongoose = require('mongoose');
var Schema = mongoose.Schema;
if (mongoose.connection.readyState === 0) {
	mongoose.connect(require('./connection-string'));
}


var tokenSchema = new Schema({
	'userToken': {
		type: String
	},
	'token': {
		type: String,
	},
	'deviceToken': {
		type: String
	},
	'source':{
		type:String,
		default:"Web"
	},
	'createdAt': {
		type: Date,
		default: Date.now
	},
	'updatedAt': {
		type: Date,
		default: Date.now
	}
});

tokenSchema.pre('save', function (next) {
	this.updatedAt = Date.now();
	next();
});

tokenSchema.pre('update', function () {
	this.update({}, {
		$set: {
			updatedAt: Date.now()
		}
	});
});

tokenSchema.pre('findOneAndUpdate', function () {
	this.update({}, {
		$set: {
			updatedAt: Date.now()
		}
	});
});

module.exports = mongoose.model('token', tokenSchema);
