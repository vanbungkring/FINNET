var models = require('../models/all-models');
var stream = require("stream");
var mailSender = require('../middleware/mailSender');
var async = require('async');
var transactionController = {
	create: function (req, res) {
		async.waterfall([
      function (callback) {
				models.customer.findOne({
					"userToken": req.body.parameters.customerToken
				}, function (err, customerResult) {
					if (!err) {
						if (!customerResult) {
							callback(null, null);
						} else {
							req.app.settings.gateway.customer.find(customerResult.braintreeCustomerId, function (err, customer) {
								callback(null, customer ? customer : null);
							});
						}
					}
				})
      },
      function (customer, callback) {
				console.log(req.body);

				models.submerchant.findOne({
					"masjidId": req.body.parameters.submerchantId
				}, function (err, result) {
					console.log(result);
					if (result) {
						var prePayment = new models.prePayment();
						prePayment.customer.token = req.body.parameters.customerToken;
						prePayment.customer.firstname = req.body.parameters.customerFirstName;
						prePayment.customer.lastName = req.body.parameters.customerLastName;
						prePayment.customer.email = req.body.parameters.customerEmail;
						prePayment.customer.id = req.body.parameters.customerId;
						prePayment.customer.source = req.body.parameters;

						prePayment.payment.amount = req.body.parameters.paymentAmount;
						prePayment.payment.name = req.body.parameters.paymentName;
						prePayment.payment.type = req.body.parameters.paymentType;
						prePayment.payment.remarks = req.body.parameters.paymentRemarks;
						prePayment.payment.fee = req.body.parameters.paymentServiceFee;
						prePayment.payment.braintreeToken = req.body.parameters.paymentBraintreeToken;
						prePayment.payment.nonce = req.body.parameters.payment_method_nonce;
						prePayment.payment.projectId = req.body.parameters.projectId ? req.body.parameters.projectId : "-";
						prePayment.payment.projectName = req.body.parameters.projectName ? req.body.parameters.projectName : "-";
						prePayment.payment.id = req.body.parameters.paymentId;

						prePayment.submerchant.braintreeid = result.btSubmerchantId ? result.btSubmerchantId : "";
						prePayment.submerchant.name = req.body.parameters.submerchantName;
						prePayment.submerchant.id = req.body.parameters.submerchantId;
						prePayment.save(function (err, result) {
							console.log(result);
						});
						req.app.settings.gateway.transaction.sale({
							amount: req.body.parameters.paymentAmount,
							merchantAccountId: result.btSubmerchantId ? result.btSubmerchantId : "",
							paymentMethodNonce: req.body.parameters.payment_method_nonce,
							customer: {
								firstName: customer ? customer.firstName : req.body.parameters.firstName,
								email: customer ? customer.email : req.body.parameters.email,
								phone: customer ? customer.phone : req.body.parameters.phone,
							},
							customFields: {
								submerchantname: req.body.parameters.submerchantName,
								submerchantid: req.body.parameters.submerchantId,
								projectid: req.body.parameters.projectId ? req.body.parameters.projectId : "",
								projectname: req.body.parameters.projectName ? req.body.parameters.projectName : "",
								paymentremarks: req.body.parameters.paymentRemarks,
								paymentname: req.body.parameters.paymentName ? req.body.parameters.paymentName : "",
								paymentid: req.body.parameters.paymentId,
								customerid: req.body.parameters.customerId,
								usertoken: req.body.parameters.customerToken,
								paymenttype: req.body.parameters.paymentType
							},
							serviceFeeAmount: req.body.parameters.paymentServiceFee
						}, function (err, transactionResult) {
							console.log(transactionResult);
							var dataResult = {};
							
							if (transactionResult.errors) {
								if (transactionResult.transaction) {
									transactionResult.transaction["amount"] = parseInt(transactionResult.transaction["amount"]) ? parseInt(transactionResult.transaction["amount"]) : 0.0;
									transactionResult.transaction.braintreeid = transactionResult.transaction["id"];
									var postPayment = new models.postPayment(transactionResult.transaction);
									postPayment.braintreeid = transactionResult.transaction.id;
									postPayment.save();
									dataResult.status = "Error";
									dataResult.invoiceRendering = {};
									dataResult.invoiceRendering.amount = result.amount;
									dataResult.invoiceRendering.clientName = result.customer.firstName;
									dataResult.invoiceRendering.email = result.customer.email;
									dataResult.invoiceRendering.status = result.status;
									dataResult.masterMerchant = result.masterMerchantAccountId;
									dataResult.subMerchantAccountId = result.subMerchantAccountId;
									dataResult.invoiceRendering.id = result._id
									dataResult.braintreeid = result.braintreeid;
									res.json(dataResult);
								} else {
									dataResult.status = "Error";
									dataResult.invoiceRendering = {};
									dataResult.invoiceRendering.status = result.status;
								}

							} else {
								transactionResult.transaction["amount"] = parseInt(transactionResult.transaction["amount"]);
								transactionResult.transaction.braintreeid = transactionResult.transaction["id"];
								var postPayment = new models.postPayment(transactionResult.transaction);
								postPayment.braintreeid = transactionResult.transaction.id;
								postPayment.save(function (err, result) {

									if (!err) {
										dataResult.status = "Success";
										dataResult.invoiceRendering = {};
										dataResult.remarks = req.body.parameters.paymentRemarks;
										dataResult.invoiceRendering.amount = result.amount;
										dataResult.invoiceRendering.clientName = result.customer.firstName;
										dataResult.invoiceRendering.email = result.customer.email;
										dataResult.invoiceRendering.status = result.status;
										dataResult.masterMerchant = result.masterMerchantAccountId;
										dataResult.subMerchantAccountId = result.subMerchantAccountId;
										dataResult.invoiceRendering.id = result._id
										dataResult.braintreeid = result.braintreeid;
										res.json(dataResult);
									}
								});
								mailSender.sendSucccessEmail(req, postPayment);
							}
						})
					}
				})
      }
    ])
	},
	transactionHistory: function (req, res) {
		async.waterfall([
      function (callback) {
				models.customer.findOne({
					"userToken": req.body.parameters
				}, function (err, customerResult) {
					if (!err) {
						if (customerResult) {
							req.app.settings.gateway.customer.find(customerResult.braintreeCustomerId, function (err, customer) {
								console.log(customer);
								callback(null, customer);
							});
						}
					}
				})
      },
      function (customer, callback) {
				var streambt = req.app.settings.gateway.transaction.search(function (search) {
					var email = customer.email;
					search.customerEmail().is(email.toString());
				});
				// Use this to find the length of the Stream or no of objects
				// before you extract it into a csv
				streambt.on("error", function () {
					console.log(streambt.searchResponse);
				});
				streambt.on("ready", function () {
					//console.log(streambt.searchResponse);
				});

				// You can either create a Array of JSON objects for the transactions
				var transactions = [];
				streambt.on("data", function (transaction) {
					transactions.push(transaction);
				});

				streambt.on("end", function () {
					transactions.forEach(function (el, index) {
						transactions[index]["amount"] = parseInt(transactions[index]["amount"]);
						transactions[index].braintreeid = transactions[index]["id"];
						delete transactions[index]["id"];
						models.postPayment.findOne({
							"braintreeid": transactions[index].braintreeid
						}, function (err, result) {
							if (!result) {
								var postPayment = new models.postPayment(transactions[index]);
								postPayment.save(function (err) {})
							}
						})

					})
					res.json(transactions);
				})
			}
    ])
	},
	searchTransactionById: function (req, res) {
		console.log(req.body);
		models.postPayment.findOne({
			"braintreeid": req.body.braintreeId
		}).exec(function (err, result) {
			if (!err) {
				console.log(result);
				res.json(result);
			}
		})
	},
	searchTransaction: function (req, res) {
		console.log(req.body);
		models.postPayment.find(req.body).exec(function (err, result) {
				if (!err) {
					console.log(result);
					res.json(result);
				} else {
					console.log(err);
					res.json(err);
				}
			})
			// models.postPayment.find(req.body}).lean(function (err, result) {
			// 	if (!err) {
			// 		console.log(result);
			// 		res.json(result);
			// 	} else {
			// 		console.log(err);
			// 		res.json(err);
			// 	}
			// })
	},
	listPrepayment: function (req, res) {
		models.prePayment.find({}).lean().exec(function (err, result) {
			if (!err) {
				console.log(result);
				res.render('payment/prePayment', {
					title: 'Pre Payment transactions',
					data: result
				});
			}
		})
	},
	listPostPayment: function (req, res) {
		models.postPayment.find({}).lean().exec(function (err, result) {
			if (!err) {
				console.log(result);
				res.render('payment/postPayment', {
					title: 'Post Payment transactions',
					data: result
				});
			}
		})
	}
}
module.exports = transactionController;
