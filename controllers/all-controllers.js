var fs = require('fs');

//require all the models
var controllers = {};
var names = fs.readdirSync('./controllers');

names.forEach(name => {
	if (!name.match(/\.js$/)) return;
	if (name === 'all-controllers.js') return;
	var model = require('./' + name);

	controllers[name.replace(".js", "")] = model;
});

module.exports = controllers;
