var models = require('../models/all-models');
var async = require('async');
var state = "auth";
var appController = {
	index: function (req, res) {
		if (!req.isAuthenticated()) {
			console.log(req.isAuthenticated());
			res.render('auth/login', {
				title: "Login",
				state: state
			});
		} else {
			res.redirect('/dashboard');
		}
	},
	dashboard: function (req, res) {
		console.log(req.app.locals.settings);
		async.parallel([
			function (callback) {
				callback(null, null)
			},
			function (callback) {
				callback(null, null)
			}
		], function (err, result) {
			res.render('dashboard/index', {
				title: 'Dashboard',
			});
		})

	}
}
module.exports = appController;
