var models = require('../models/all-models');
var tokenControllers = {
	create: function (req, res) {
		req.app.settings.gateway.webhookNotification.parse(
			req.body.bt_signature,
			req.body.bt_payload,
			function (err, webhookNotification) {
				console.log(webhookNotification);
				if (!err) {
					if (webhookNotification.kind === req.app.settings.braintree.WebhookNotification.Kind.SubMerchantAccountApproved) {
						models.submerchant.findOne({
							"btSubmerchantId": webhookNotification.merchantAccount.id
						}, function (err, result) {
							result.btApprovalStatus = webhookNotification.merchantAccount.status;
							result.save();
						})
					} else if (webhookNotification.kind === req.app.settings.braintree.WebhookNotification.Kind.SubMerchantAccountDeclined) {

					}
				}
			}
			//console.log("[Webhook Received " + webhookNotification.timestamp + "] | Kind: " + webhookNotification.kind + " | Subscription: " + webhookNotification.subscription.id);
		);
		res.send(200);
	}
}
module.exports = tokenControllers;
