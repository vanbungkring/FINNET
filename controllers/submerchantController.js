var models = require('../models/all-models');
var util = require('../middleware/util');
var moment = require('moment');
var braintree = require('braintree');
var submerchantController = {
	create: function (req, res) {
		var merchantAccountParams = {
			individual: {
				firstName: req.body.parameters.individualContactPersonFirstName,
				lastName: req.body.parameters.individualcontactPersonLastName,
				email: req.body.parameters.individualEmail,
				dateOfBirth: req.body.parameters.individualDateOfBirth, //"1981-11-19",
				address: {
					streetAddress: req.body.parameters.individualstreetAddress,
					locality: req.body.parameters.individualLocality,
					region: req.body.parameters.individualRegion,
					postalCode: req.body.parameters.individualPostalCode,
				}
			},
			business: {
				legalName: req.body.parameters.legalName,
				dbaName: req.body.parameters.legalName,
				taxId: req.body.parameters.taxId,
				address: {
					streetAddress: req.body.parameters.streetAddress,
					locality: req.body.parameters.locality,
					region: req.body.parameters.bussinessRegion,
					postalCode: req.body.parameters.postalCode
				}
			},
			funding: {
				destination: req.app.settings.braintree.MerchantAccount.FundingDestination.Bank,
				accountNumber: req.body.parameters.bankAccountNumber,
				routingNumber: req.body.parameters.bankRoutingNumber
			},
			tosAccepted: true,
			masterMerchantAccountId: "UDonate",
			id: util.sanitizeSubmerchantId(req.body.parameters.legalName) + '_' + Math.round(+new Date() / 1000)
		};
		req.app.settings.gateway.merchantAccount.create(merchantAccountParams, function (err, result) {
			if (result.success) {
				console.log(req.body.parameters.masjidId);
				console.log(result.merchantAccount.id);
				console.log(result.merchantAccount.status);
				var submerchant = new models.submerchant();
				submerchant.masjidId = req.body.parameters.masjidId;
				submerchant.btApprovalStatus = result.merchantAccount.status;
				submerchant.btSubmerchantId = result.merchantAccount.id;
				submerchant.save(function (err, result) {
					req.app.settings.output.successResult(res, "success", result);
				});
			} else {
				req.app.settings.output.successResult(res, "error", result.errors.deepErrors());
			}
		});
	},
	list: function (req, res) {
		models.submerchant.find({}, function (err, result) {
			res.render('submerchant/index', {
				title: 'Submerchant',
				data: result
			});
		})
	},
	read: function (req, res) {
		if (req.method == 'POST') {
			console.log(req);
		}
	}
}
module.exports = submerchantController;
