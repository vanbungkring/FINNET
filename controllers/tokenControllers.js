var models = require('../models/all-models');
var tokenControllers = {
	create: function (req, res) {
		req.app.settings.gateway.clientToken.generate({}, function (err, response) {
			console.log(req.body.parameters);
			var token = new models.token({
				token: response.clientToken,
				source: response.source,
				userToken: req.body.parameters.token ? req.body.parameters.token : "",
				deviceToken: req.body.parameters.deviceToken ? req.body.parameters.deviceToken : "",
			});
			token.save(function (err, result) {
				if (err) {
					//req.app.settings.output.errorResult(res, 500, result);
				} else {
					console.log(req.session.state);
					if (req.session.state === undefined) {
						req.app.settings.output.successResult(res, "success", result);
					}

				}
			})

		})
	},
	list: function (req, res) {
		models.token.find({}).lean().exec(function (err, result) {
			if (!err) {
				res.render('token/index', {
					title: 'Dashboard',
					data: result
				});
			}
		})
	}
}
module.exports = tokenControllers;
