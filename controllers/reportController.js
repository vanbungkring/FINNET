var models = require('../models/all-models');

var reportController = {
	reportControllerOnDemand: function (req, res) {
		console.log(req.body);
		var data;
		var customAdder = "";

		var matcher = [{
				'status': "authorized"
						},
			{
				'createdAt': {
					$gte: req.body.startDate,
					$lt: req.body.endDate
				}
					},
			{
				'customFields.paymentid': {
					$in: req.body.paymentList
				}
					}]
		if (req.body.masjidId != '0') {
			data = {
				'customFields.submerchantid': req.body.masjidId
			};
			matcher.push(data);
		}
		models.postPayment.aggregate([
				{
					$match: {
						$and: matcher
					}
				    }, {
					$group: {
						_id: "$customFields.paymentid",
						total: {
							$sum: "$amount"
						}
					}
						}],
			function (err, result) {
				if (err) {
					res.json(err);
				} else { console.log(result);
					res.json(result);
				}
			})
	},
	transactionByStatus: function (req, res) {
		models.postPayment.aggregate([
      [{
				$group: {
					_id: "$status",
					total: {
						$sum: 1
					},
					amount: {
						$sum: "$amount"
					}
				}
			}]
    ], function (err, result) {
			res.json(result)
		})
	}
}
module.exports = reportController;
