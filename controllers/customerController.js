var models = require('../models/all-models');
var customerController = {
	create: function (req, res) {
		req.app.settings.gateway.customer.create({
			firstName: req.body.parameters.name,
			email: req.body.parameters.email,
			phone: req.body.parameters.phone ? req.body.parameters.phone : "",
		}, function (err, result) {
			if (!err) {
				var customer = new models.customer();
				customer.userToken = req.body.parameters.token;
				customer.braintreeCustomerId = result.customer.id;
				customer.braintreeMerchantId = result.customer.merchantId;
				customer.save();
			} else {
				req.app.settings.output.successResult(res, result);
			}
		});
	},
	list: function (req, res) {
		models.customer.find({}).lean().exec(function (err, result) {
			if (!err) {
				res.render('customer/index', {
					title: 'Customer',
					data: result
				});
			}
		})
	}
}
module.exports = customerController;
